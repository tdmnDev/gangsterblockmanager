﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MissionPageController : MonoBehaviour {

    public GameObject[] m_aMissionPanels;
    public MissionManager m_MissionManager;

	// Use this for initialization
	void Start ()
    {
        DB_MissionParamsContainer dbMissionParamsContainer = DB_MissionParamsContainer.LoadFromStaticPath();
        if (m_aMissionPanels.Length == dbMissionParamsContainer.vMissionParams.Count)
        {
            byte tmpCounter = 0;
            foreach(var mission in dbMissionParamsContainer.vMissionParams)
            {
                m_aMissionPanels[tmpCounter].transform.FindChild("TitleMissionPanelText").gameObject.GetComponent<Text>().text = mission.MissionName;
                string description = m_aMissionPanels[tmpCounter].transform.FindChild("DescMissionPanelText").gameObject.GetComponent<Text>().text;
                description = string.Format(description, 
                    mission.NumControlledBuilding, 
                    (mission.MinOwnership*100),
                    mission.NumOwnedBuilding,
                    mission.MonthDuration
                    );
                m_aMissionPanels[tmpCounter++].transform.FindChild("DescMissionPanelText").gameObject.GetComponent<Text>().text = description;
            }
        }
        
	}
	
}
