﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuildingStatsTutorial : MonoBehaviour {

    public Sprite[] BuildingStatSprite;
    public GameObject Image;
    private Image privImage;
    private int index = 0;
    
	// Use this for initialization
	void Start () {
        privImage = Image.GetComponent<Image>();
        privImage.sprite = BuildingStatSprite[index];
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	    


	}

    public void NextImageStat()
    {
        Debug.Log(BuildingStatSprite.Length);
        index +=1;
        if (index >= BuildingStatSprite.Length) { index = 0; }
        privImage.sprite = BuildingStatSprite[index];
    
    }

    public void PrevImageStat() 
    {
        Debug.Log(BuildingStatSprite.Length);
        index -= 1;
        if (index < 0) { index = BuildingStatSprite.Length; }
        privImage.sprite = BuildingStatSprite[index];
    }

}
