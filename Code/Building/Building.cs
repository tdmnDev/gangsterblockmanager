﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class Building : MonoBehaviour {

    //Objects
    public BuildingStatus bStatus;
    public GameObject gManager;
    private Controller controller;
    public Sprite icon;
    public GameObject SpriteIcon;

    //Property
    public GameObject Door;
    private List<GameObject> Doors;
    private int nDoors = 4 ;
    public GameObject Light;
    
    //Hour
    public int openHour;
    public int closeHour;
    public bool open= false;
    public float hourCost;
    
    //Appeal
    public float storeAppeal = 0.5f;
    public int storeAppealUpgrade;
    public int AppealUpgradeCounter = 0;
    private float storeAppealModifier = 0;
    public float effectiveAppeal;

    //Slots
    public GameObject FellaSlots;
    public GameObject EnemySlots;
    public Transform[] FellaSlotPosition;
    public Transform[] EnemySlotPosition;
    
    //Upgrade
    public bool FbPageActive = false;
    public bool MascotteActive = false;
    
    


	// Use this for initialization
	void Start () {

        

        Light.SetActive(false);
        controller = gManager.GetComponent<Controller>();

        FellaSlotPosition = FellaSlots.GetComponentsInChildren<Transform>();
        EnemySlotPosition = EnemySlots.GetComponentsInChildren<Transform>();

        IconSetup();

        Doors = new List<GameObject>();

        float[] xD = { 0.6f, -0.6f,0f,0f };
        float[] zD = { 0, 0, 0.6f, -0.6f };

        for (int i = 0; i < nDoors; i++)
        {
            GameObject g = (GameObject)Instantiate(Door, transform.position, Quaternion.identity);
            g.transform.parent = transform;
            g.transform.localPosition = new Vector3( xD[i], 0,  zD[i]);

            Doors.Add(g);
        }

        
        bStatus.UpdateTaxes();

        storeAppealUpgrade = Mathf.RoundToInt(bStatus.BuildingValue/4);
        effectiveAppeal = storeAppeal;

        StaticMap.AddBuilding(gameObject);

	}

    private void IconSetup()
    {
        //Icon Setup
        GameObject Icon = (GameObject)Instantiate(SpriteIcon, Vector3.zero, Quaternion.identity);
        Icon.transform.parent = transform;
        Icon.GetComponent<SpriteRenderer>().sprite = icon;
        Icon.transform.localPosition = new Vector3(0,1,0);
        Icon.transform.rotation = Quaternion.EulerAngles(new Vector3(90,0,0));
        
    }
	
	// Update is called once per frame
	void Update () {
        OpenCloseStore();
	}


    void OnMouseDown()
    {
        controller.HandleBuildingClick(this);
    }

    public Transform GetDoor()
    {
        return Doors[Random.Range(0,3)].transform;
    }

    public Transform GetDoor(Transform spawnPosition) 
    {
        if(spawnPosition.position.x > transform.position.x)
        {
            if (spawnPosition.position.z > transform.position.z)
            {
                // Top || Right
                return Doors[2].transform;
            }
            else
            {
                //Bottom || Right
                return Doors[0].transform;
            }
        }
        else
        {
            if (spawnPosition.position.z > transform.position.z)
            {
                // Top || Left
                return Doors[1].transform;
            }
            else
            {
                //Bottom || Left
                return Doors[3].transform;
            }
        }

        return null;
    }

    public void SelectLight()
    {
        if (Light.activeSelf)
        {
            
            Light.SetActive(false);

        }
        else
        {
            
            Light.SetActive(true);
        }
    }


    internal void DisposeFellas()
    {
        for (int i = 0; i < bStatus.nSecurityFells; i++)
        {
            bStatus.securityFellas[i].transform.position = FellaSlotPosition[i+1].position;
        }
    }

    internal GameObject GetFellaTarget(int EnemyIndex)
    {
        return bStatus.securityFellas[EnemyIndex];
    }

    private void OpenCloseStore()
    {
        if (openHour > closeHour)
        {
            if (!(gManager.GetComponent<GameManager>().hourCounter < openHour && gManager.GetComponent<GameManager>().hourCounter > closeHour))
            {
                open = true;
            }
            else
            {
                open = false;
            }

        }

        if (openHour < closeHour)
        {
            if ((gManager.GetComponent<GameManager>().hourCounter > openHour  && gManager.GetComponent<GameManager>().hourCounter < closeHour))
            {
                open = true;
            }
            else
            {
                open = false;
            }
        }
    }

    public int GetMontlyCost() 
    {

        return Mathf.RoundToInt(StaticMap.CalculateTotOpenHour(openHour, closeHour) * hourCost * gManager.GetComponent<GameManager>().monthLen[gManager.GetComponent<GameManager>().monthIndex]);

    }


    internal void ApplyModifier(float p)
    {
        effectiveAppeal = storeAppeal + p;
    }

    internal bool HandleFbUpgrade()
    {
        if (FbPageActive)
        {
            return TurnFbPageOff();
        }
        else
        {
            return TurnFbPageOn();
        }
    }

    private bool TurnFbPageOn()
    {
        if (bStatus.CleanCash > 5) 
        {
            effectiveAppeal += 0.05f;
            hourCost += 0.1f;
            bStatus.CleanCash -= 5;
            FbPageActive = true;
            return true;
        }
        return false;

    }

    private bool TurnFbPageOff()
    {
        effectiveAppeal -= 0.05f;
        hourCost -= 0.1f;
        FbPageActive = false;
        return true;
    }

    public bool HandleMascotteUpgrade()
    {
        if (MascotteActive)
        {
            return TurnMascotteOff();
        }
        else
        {
            return TurnMascotteOn();
        }
    }

    private bool TurnMascotteOn()
    {
        if (bStatus.CleanCash > 10)
        {
            effectiveAppeal += 0.1f;
            hourCost += 0.2f;
            bStatus.CleanCash -= 10;
            MascotteActive = true;
            return true;
        }
        return false;

    }

    private bool TurnMascotteOff()
    {
        effectiveAppeal -= 0.1f;
        hourCost -= 0.2f;
        MascotteActive = false;
        return true;
    }

}
