﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class BuildingStatus : MonoBehaviour {

    public enum BuildingCondition{Indie, Controlled, Owned, Closed};

    public float BlackMoneyProbability;
    public float CleanMoneyProbability;
    
    public GameObject PlayerController;
    public int nSecurityFells;
    public List<GameObject> securityFellas;
    public int FellasRequired;


    public int BlackCash;
    public int CleanCash;

    public int TotalRecivedBlack=0;
    public int TotalRecivedClean=0;
    public int AvgRecivedBlack;
    public int AvgRecivedClean;
    public int Sold=0;

    public int AvgCost;
    public int MaxAvgCost;
    public int MinAvgCost;

    public int BuildingCost;
    public int BuildingValue;
    public float TotalBuildingTax;
    public float TaxAmount;
    public float TaxPercentage = 0.2f;
    

    public BuildingCondition InternalStatus = BuildingCondition.Indie;
    public float Ownership = 0;

    private string buildingName;
    public bool Active = true;

    public Building buildingComponent;

    //Debug for balance;
    public int dailyDeals=0;
    public int dayCounter=0;
    public float AvgDeals = 0;



	// Use this for initialization
	void Start () {
        buildingName = gameObject.name;
        securityFellas = new List<GameObject>(nSecurityFells);
        buildingComponent = gameObject.GetComponent<Building>();

        MaxAvgCost = Mathf.RoundToInt(AvgCost + AvgCost * 0.3f);
        MinAvgCost = Mathf.RoundToInt(AvgCost - AvgCost * 0.3f);


        UpdateTaxes();
	}
	
	// Update is called once per frame
	void Update () 
    {
        TurnState();
	}


    private void TurnState()
    {
        if (CleanCash >= 0  )
        {
            FellasRequired = nSecurityFells - securityFellas.Count;
            if (securityFellas.Count < nSecurityFells)
            {
                if (PlayerController != null)
                { gameObject.GetComponent<Renderer>().material.color = Color.white; }
                InternalStatus = BuildingCondition.Indie;
                PlayerController = null;

            }

            if (securityFellas.Count == nSecurityFells)
            {
                if (Ownership == 0)
                {
                    InternalStatus = BuildingCondition.Controlled;
                    if (PlayerController == null)
                    {
                        PlayerController = securityFellas[0].transform.parent.gameObject;
                        gameObject.GetComponent<Renderer>().material.color = PlayerController.GetComponent<PlayerScript>().playerColor;
                    }
                }
                else
                {
                    if (PlayerController == null)
                    {
                        PlayerController = securityFellas[0].transform.parent.gameObject;
                        gameObject.GetComponent<Renderer>().material.color = PlayerController.GetComponent<PlayerScript>().playerColor;
                        Active = true;
                    }
                    InternalStatus = BuildingCondition.Owned;
                }

            }
        }
        else
        {
            if (InternalStatus != BuildingCondition.Closed)
            {
                InternalStatus = BuildingCondition.Closed;
                Active = false;
                if (securityFellas.Count > 0 && PlayerController != null) 
                {
                    ReleaseFellas(); PlayerController = null;
                }
                gameObject.GetComponent<Renderer>().material.color = Color.red;
                UpdateTaxes();
            }
        }
    }

    private void ReleaseFellas()
    {
        PlayerController.GetComponent<PlayerScript>().RecallFellasFromBuilding(buildingComponent);
    }

    public void ResetDailyDeals()
    {
        dayCounter++;
        AvgDeals = (AvgDeals * (dayCounter - 1) + dailyDeals) / dayCounter;
        dailyDeals = 0;
    }

    public bool RecivePayment(int blackMoney, int cleanMoney)
    {
        if (blackMoney > 0 || cleanMoney > 0)
        {
            dailyDeals++;

            TaxAmount += ((cleanMoney) * TaxPercentage);
            //Update totalTaxes
            UpdateTaxes();


            TotalRecivedBlack += blackMoney;
            TotalRecivedClean += cleanMoney;

            if (securityFellas.Count == nSecurityFells)
            {
                PayBoss(blackMoney, cleanMoney);
            }
            else
            {
                BlackCash += blackMoney;
                CleanCash += cleanMoney;
            }

            Sold++;

            AvgRecivedBlack = TotalRecivedBlack / Sold;
            AvgRecivedClean = TotalRecivedClean / Sold;
            return true;
        }
        return false;
    }

    public void UpdateTaxes()
    {
        UpdateCosts();
        if (InternalStatus != BuildingCondition.Closed)
        {
            TotalBuildingTax = TaxAmount + BuildingCost;
        }
        else
        {
            TotalBuildingTax = 0; 
        }
        
    }

    public void UpdateCosts()
    {
        BuildingCost = Mathf.RoundToInt(buildingComponent.GetMontlyCost() * (1 - Ownership));
    }

    void PayBoss(int blackMoney, int cleanMoney)
    { 
        //
        BlackCash += (blackMoney/2);
        CleanCash += (int)((cleanMoney / 2) + (cleanMoney / 2) * (1 - Ownership));


        if (PlayerController != null) 
        {
            PlayerScript ps = PlayerController.GetComponent<PlayerScript>();
            ps.transform.parent.GetComponent<GUImanager>().AddNewLine("Building " + gameObject.name + " give us--> Black : " + (blackMoney / 2) + ", Clean : " + ((int)(Mathf.Round((cleanMoney) * Ownership))) + " = (" + (((cleanMoney) * Ownership)) + ") = (" + cleanMoney + " *" + Ownership + ")");
            ps.RecivePayment(blackMoney/2, (int)(Mathf.Round((cleanMoney)*Ownership)));
        }
    }

    public void PayTaxes()
    {
                        //TaxAmount                 //BuildingCost                                      //HourCost
        //CleanCash -= Mathf.RoundToInt(TaxAmount) + Mathf.RoundToInt(BuildingCost ) + ;

        CleanCash -= Mathf.RoundToInt(TotalBuildingTax);
        TaxAmount = 0;
        UpdateTaxes();

    }


    
}
