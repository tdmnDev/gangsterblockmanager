﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeathSound : MonoBehaviour {

    public AudioSource[] AudioSequence;
    public GameObject reason;
    int i = 0;

	// Use this for initialization
	IEnumerator Start () {

        GameObject mm = GameObject.Find("MissionManager");

        reason.GetComponent<Text>().text = mm.GetComponent<MissionManager>().DeathReason;

	    AudioSequence[i].Play();
        while(i < AudioSequence.Length-1)
        {
            if(!AudioSequence[i].isPlaying )
            {
                AudioSequence[++i].Play();
            }
            yield return null;
        }

	}
	
	
}
