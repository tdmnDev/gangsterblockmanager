﻿using UnityEngine;
using System.Collections;

public class BloodSplat : MonoBehaviour {

    public ParticleSystem blood;

	// Use this for initialization
	IEnumerator Start () {
        blood.Play();
        while (blood.isPlaying) { yield return null; }

        Destroy(gameObject);

    }
	
}
