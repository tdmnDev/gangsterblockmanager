﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class CitizenSpawner : AgentSpawner
{
    
    public int MaxCitizen = 10;


    IEnumerator Start() 
    {
        AgentPool = new List<GameObject>();

        spawnPoints = SpawningPointRoot.GetComponentsInChildren<Transform>();

        //Populate pool
        while (AgentPool.Count < MaxCitizen)
        {
            if (StaticMap.MapReady)
            {
                yield return StartCoroutine(AddCitizenToPool());
            }
        }

        //Spawning loop
        while (Active)
        {
            yield return StartCoroutine(SpawnCitizen());
            yield return new WaitForSeconds(SpawnFrequence);
        }

        yield return null;
    }

    IEnumerator AddCitizenToPool() 
    {

        GameObject g = (GameObject)Instantiate(AgentPrefab, transform.position, Quaternion.identity);
        AgentPool.Add(g);
        g.transform.parent = AgentsRoot.transform;

        yield return null;
    }

    IEnumerator SpawnCitizen()
    {
        int index=0;

        //Check for citizen available
        while(index < MaxCitizen){
            
            //If an agent is active
            if (!AgentPool[index].activeInHierarchy)
            {
                //Check for a building open now
                Building b = StaticMap.GetRandomOpenBuilding(gManager.hourCounter);
                if (b != null)
                {
                    Transform pos = spawnPoints[Random.Range(1, spawnPoints.Length)];

                    //Reset unactive agent to respawn it
                    AgentPool[index].transform.position = pos.position;
                    AgentPool[index].GetComponent<BaseAgent>().startingPosition = pos;
                    AgentPool[index].GetComponent<BaseAgent>().Icon = b.icon;
                    AgentPool[index].SetActive(true);
                    AgentPool[index].GetComponent<BaseAgent>().TurnOn(b.GetDoor(pos));
                    break; // <--
                }
                
            }
            index++;
            yield return null;
        }
    }



}
