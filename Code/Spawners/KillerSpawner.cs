﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class KillerSpawner : MonoBehaviour {


    Transform[] spawnPoints;
    public GameObject Killer;
    public GameManager gManager;

    public int MaxKiller = 10;
    public float NightFrequence;
    public float DayFrequence;
    public float SpawnFrequence;

    List<GameObject> KillerPool;

    bool Active = true;

    IEnumerator Start()
    {
        KillerPool = new List<GameObject>();

        spawnPoints = GetComponentsInChildren<Transform>();

        
        while (Active)
        {
            yield return new WaitForSeconds(SpawnFrequence * 10);
            yield return StartCoroutine(SpawnKiller());
            
        }


        yield return null;
    }

    void Update()
    {
        if (gManager.hourCounter == 22)
        {
            SpawnFrequence = NightFrequence;
        }

        if (gManager.hourCounter == 7)
        {
            SpawnFrequence = DayFrequence;
        }
    }

    

    IEnumerator SpawnKiller()
    {
        

        Transform startingPos = spawnPoints[Random.Range(1, spawnPoints.Length)];

        Transform exitPos = startingPos;

        while (exitPos == startingPos)
        {
            exitPos = spawnPoints[Random.Range(1, spawnPoints.Length)];
        }
        

        GameObject KillerInstance = (GameObject)Instantiate(Killer, transform.position, Quaternion.identity);
        KillerInstance.transform.parent = transform;

        //Get Random Entry
        KillerInstance.transform.position = startingPos.position;
        KillerInstance.GetComponent<KillerAgentScript>().startingPosition = startingPos;

        //Get Random Exit
        KillerInstance.GetComponent<KillerAgentScript>().target = exitPos;

        //Activate
        KillerInstance.SetActive(true);

        //Walk
        KillerInstance.GetComponent<KillerAgentScript>().TurnOn();


        yield return null;


    }
}
