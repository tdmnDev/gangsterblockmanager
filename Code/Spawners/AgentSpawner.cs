﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AgentSpawner : MonoBehaviour {

    //Cached Objects
    public GameObject AgentPrefab;
    public GameManager gManager;
    public Transform AgentsRoot;
    public GameObject SpawningPointRoot;

    protected Transform[] spawnPoints;
    protected List<GameObject> AgentPool;


    //Spawning Parameters
    protected bool Active = true;
    public float NightFrequence;
    public float DayFrequence;
    public float SpawnFrequence;

    //Private parameters
    private int firstNightHour = 22;
    private int firstDayHour = 7;

    void Update()
    {
        ChangeCurrentSpawnFrequence();
    }

    protected void ChangeCurrentSpawnFrequence()
    {
        if (gManager.hourCounter == firstNightHour)
        {
            SpawnFrequence = NightFrequence;
        }
        else
        if (gManager.hourCounter == firstDayHour)
        {
            SpawnFrequence = DayFrequence;
        }
    }
}
