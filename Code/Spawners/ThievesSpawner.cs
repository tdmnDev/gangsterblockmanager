﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class ThievesSpawner : AgentSpawner
{
    public int MaxThief = 10;

    IEnumerator Start()
    {
        AgentPool = new List<GameObject>();

        spawnPoints = GetComponentsInChildren<Transform>();



        while (Active)
        {
            yield return new WaitForSeconds(SpawnFrequence*10);
            yield return StartCoroutine(SpawnThief());
            
        }


        yield return null;
    }

    IEnumerator SpawnThief()
    {


        Transform startingPos = spawnPoints[Random.Range(1, spawnPoints.Length)];

        Transform exitPos = startingPos;

        while (exitPos == startingPos)
        {
            exitPos = spawnPoints[Random.Range(1, spawnPoints.Length)];
        }


        GameObject ThiefInstance = (GameObject)Instantiate(AgentPrefab, transform.position, Quaternion.identity);
        ThiefInstance.transform.parent = transform;

        //Get Random Entry
        ThiefInstance.transform.position = startingPos.position;
        ThiefInstance.GetComponent<ThiefAgentScript>().startingPosition = startingPos;

        //Get Random Exit
        ThiefInstance.GetComponent<ThiefAgentScript>().target = exitPos;

        //Activate
        ThiefInstance.SetActive(true);

        //Walk
        ThiefInstance.GetComponent<ThiefAgentScript>().TurnOn();


        yield return null;


    }


}
