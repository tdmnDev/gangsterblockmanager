﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class EnemySpawner : MonoBehaviour
{

    Transform[] spawnPoints;
    public GameObject Enemy;
    public int MaxEnemy = 10;

    List<GameObject> EnemyPool;

    bool Active = true;

    IEnumerator Start()
    {
        EnemyPool = new List<GameObject>();

        spawnPoints = GetComponentsInChildren<Transform>();

        while (EnemyPool.Count < MaxEnemy)
        {
            if (StaticMap.MapReady)
            {
                yield return StartCoroutine(AddEnemyToPool());

            }
        }

        while (Active)
        {
            yield return new WaitForSeconds(Random.Range(500f, 600f)); //Random.Range(130f,200f)
            yield return StartCoroutine(Attack());
            yield return new WaitForSeconds(Random.Range(20f,40f));
            yield return StartCoroutine(RecallEnemy());
        }


        yield return null;
    }

    void Update()
    {

    }

    IEnumerator AddEnemyToPool()
    {

        GameObject g = (GameObject)Instantiate(Enemy, transform.position, Quaternion.identity);
        EnemyPool.Add(g);
        g.transform.parent = transform;

        yield return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator Attack()
    {

        Building b = null;
        Transform startingPosition = spawnPoints[Random.Range(1, spawnPoints.Length)];

        while(b==null || b.bStatus.InternalStatus == BuildingStatus.BuildingCondition.Indie )
        {
            
            b = StaticMap.GetRandomBuilding();
            

        }

        int EnemyNeed = b.bStatus.nSecurityFells;
        
        for (int i = 0; i < EnemyNeed; i++)
        {
           
            EnemyPool[i].GetComponent<EnemyAgentScript>().EnemyIndex = i;
            
            EnemyPool[i].GetComponent<EnemyAgentScript>().target = b.EnemySlotPosition[i + 1];
            
            EnemyPool[i].GetComponent<EnemyAgentScript>().transform.position = startingPosition.position;
            
            EnemyPool[i].GetComponent<EnemyAgentScript>().startingPosition = startingPosition;
            
            EnemyPool[i].SetActive(true);
            EnemyPool[i].GetComponent<EnemyAgentScript>().TurnOn();
        }

        yield return null;
        
    }


    IEnumerator RecallEnemy()
    {
        foreach (GameObject g in EnemyPool) 
        {
            if (g.activeInHierarchy && g.GetComponent<EnemyAgentScript>().Attacking) 
            {
                g.GetComponent<EnemyAgentScript>().BackToHome();
            }
        }

        yield return null;
    }

}