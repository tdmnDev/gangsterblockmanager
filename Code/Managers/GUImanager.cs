﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUImanager : MonoBehaviour {

    //GameManager
    public GameManager gManager;
    public Controller controller;

    //Boss Panel
    public GameObject BossBlackMoney;
    public GameObject BossCleanMoney;
    public GameObject BossFellasNumber;
    public GameObject BossFreeFellasNumber;
    public GameObject HireFellaDesc;
    public GameObject FireFellaDesc;

    public GameObject CoolDownWages;
    public GameObject CoolDownWagesDesc;
    public GameObject CoolDownTaxesDesc;
    public GameObject CoolDownTaxes;
    public GameObject YearsCounter;



    //Building Panel
    public GameObject BuildingPanel;
    public GameObject BuildingName;
    public GameObject BuildingStatus;
    public GameObject BuildingBlackMoney;
    public GameObject BuildingCleanMoney;
    public GameObject BuildingBlackMoneyStorage;
    public GameObject BuildingCleanMoneyStorage;
    public GameObject BuildingFellasNumber;
    public GameObject BuildingValue;
    public GameObject BuildingOwnership;
    public GameObject BuildingIcon;
    public GameObject BuildingOpenHour;
    public GameObject BuildingCloseHour;
    public GameObject BuildingAvgCost;
    public GameObject TaxMemorandum;
    public GameObject DealTaxes;
    public GameObject FixedCosts;
    public GameObject BuySellPercDesc;
    public GameObject CostPerHour;
    public GameObject MontlyProspect;
    public GameObject AppealIndex;
    public GameObject UpgradeAppealLabel;
    public GameObject[] BuildingMicroPanels;
    public GameObject[] UpgradeButtonsLabels;

    //Fella Panel
    public GameObject FellaPanel;
    public GameObject FellaName;
    public GameObject FellaStatus;
    public GameObject WeaponValue;
    public GameObject HealthValue;
    public GameObject[] FellaMicroPanels;
    public GameObject[] Upgrades;
    


    //Building Alert
    public GameObject BuildingAlert;

    //Audio Alert
    public AudioSource Voices;

    //Mission Panel
    public GameObject MissionPanel;

    //Building Status Button
    public GameObject OfferControlButton;
    public GameObject BuyTenButton;
    public GameObject WashTheirMoney;
    public GameObject SellTenButton;
    public GameObject ReleaseButton;
    public GameObject PayDebt;
    public GameObject[] ControlButtons;

    //Bottom Panel
    public GameObject[] lines;
    public int linesCounter = 0;

    //Calendar Panel
    public GameObject hour;
    public GameObject TimeScale;
    

	// Use this for initialization
	void Start () {
        BuildingPanel.SetActive(false);
	
	}
	
	// Update is called once per frame
	void Update () {
        if (gManager.player != null) 
        {
            SetBossPanelValues();
            CalendarUpdate();
        }
            
    
        if (gManager.player != null && controller.selectedBuilding != null)
        {
            SetBuildingPanelValues(controller.selectedBuilding);
        }


        
	}

    void CalendarUpdate()
    {
        TimeScale.GetComponent<Text>().text = "Time Scale : 0 <= " + (Time.timeScale) + " <= 3";
        if (gManager.hourCounter < 10)
        {
            hour.GetComponent<Text>().text = "0" + gManager.hourCounter + ".00 - "+ gManager.dayCounter +" "+gManager.getCurrentMonth() ;
        }
        else
        {
            hour.GetComponent<Text>().text = "" + gManager.hourCounter + ".00 - " + gManager.dayCounter + " " + gManager.getCurrentMonth();
        }


        
    }

    void SetBossPanelValues()
    {

        BossBlackMoney.GetComponent<Text>().text = "" + gManager.player.BlackMoney + " $B";
        BossCleanMoney.GetComponent<Text>().text = "" + gManager.player.CleanMoney + " $C";
        HireFellaDesc.GetComponent<Text>().text = "It will cost you " + (gManager.player.FellaCost) + "$B,\n + " + (gManager.player.FellaCost) + "$B per month";
        BossFellasNumber.GetComponent<Text>().text = "" + gManager.player.Fellas.Count;
        BossFreeFellasNumber.GetComponent<Text>().text = "" + gManager.player.GetFreeFellas();
        CoolDownWagesDesc.GetComponent<Text>().text = "Wages (" + (gManager.player.totalFellaCost) + "$B) in ";
        CoolDownWages.GetComponent<Text>().text = "" + ((int)(gManager.timeWagesLimit - gManager.wagesTime)) + " days";
        if ((gManager.player.Fellas.Count * 20) < gManager.player.BlackMoney)
        {
            CoolDownWages.GetComponent<Text>().color = Color.black;
            
            
        }
        else
        {
            CoolDownWages.GetComponent<Text>().color = Color.red;
            if (((int)(gManager.timeWagesLimit - gManager.wagesTime)) < 5 && !gManager.player.GetComponent<AudioSource>().isPlaying)
            {
                
                //Sound Fellas Alert
                gManager.player.CantPayFellasAudio.Play();

            }
        }

        CoolDownTaxesDesc.GetComponent<Text>().text = "Tax day (" + (gManager.player.TaxCounter) + "$C) in ";
        CoolDownTaxes.GetComponent<Text>().text = ""+((int)(gManager.timeWagesLimit - gManager.wagesTime)) + " days";
        //YearsCounter.GetComponent<Text>().text = "Year : " + ((int)gManager.years) ;
    }

    void SetBuildingPanelValues(Building building)
    {
        BuildingName.GetComponent<Text>().text = "" + building.name;
        BuildingIcon.GetComponent<Image>().sprite = building.icon;
        BuildingStatus.GetComponent<Text>().text = GetStringStatus(building);
        BuildingBlackMoney.GetComponent<Text>().text = "" + building.bStatus.BlackMoneyProbability*100 + "%";
        BuildingCleanMoney.GetComponent<Text>().text = "" + building.bStatus.CleanMoneyProbability*100 + "%";
        BuildingBlackMoneyStorage.GetComponent<Text>().text = "" + building.bStatus.BlackCash + "$B";
        BuildingCleanMoneyStorage.GetComponent<Text>().text = "" + building.bStatus.CleanCash + "$C";
        BuildingFellasNumber.GetComponent<Text>().text = "" + building.bStatus.FellasRequired;

        BuildingOpenHour.GetComponent<Text>().text = " " + building.openHour + "";
        BuildingCloseHour.GetComponent<Text>().text = " " + building.closeHour + "";
        BuildingValue.GetComponent<Text>().text = "" + building.bStatus.BuildingValue +"$C";
        BuildingOwnership.GetComponent<Text>().text = "" + (building.bStatus.Ownership *100) + "%";
        BuildingAvgCost.GetComponent<Text>().text = "" + (building.bStatus.AvgCost) + "$"; 
        
        DealTaxes.GetComponent<Text>().text = "" + (building.bStatus.TaxAmount) + "$C";
        FixedCosts.GetComponent<Text>().text = "" + (building.bStatus.BuildingCost) + "$";

        CostPerHour.GetComponent<Text>().text = "" + (building.hourCost) + "$C";
        MontlyProspect.GetComponent<Text>().text = "" + (building.bStatus.BuildingCost) + "$C";

        TaxMemorandum.GetComponent<Text>().text = "Taxes ("+ (building.bStatus.TotalBuildingTax)+") in " +  ((int)(gManager.timeWagesLimit - gManager.wagesTime)) + " days";

        AppealIndex.GetComponent<Text>().text = "" + (Mathf.RoundToInt(building.effectiveAppeal * 100)) + "%";
        UpgradeAppealLabel.GetComponent<Text>().text = "Upgrade " + (building.storeAppealUpgrade) + "$C";
        if (!building.FbPageActive)
        {
            UpgradeButtonsLabels[0].GetComponent<Text>().text = "Activate";
        }
        else
        {
            UpgradeButtonsLabels[0].GetComponent<Text>().text = "Disable";
        }

        if (!building.MascotteActive)
        {
            UpgradeButtonsLabels[1].GetComponent<Text>().text = "Activate";
        }
        else
        {
            UpgradeButtonsLabels[1].GetComponent<Text>().text = "Disable";
        }
        

    }

    string GetStringStatus(Building b) 
    { 
        switch(b.bStatus.InternalStatus)
        {
            case global::BuildingStatus.BuildingCondition.Closed:
                {
                    BuyTenButton.SetActive(false);

                    WashTheirMoney.SetActive(false);
                    ActivateControlButtons(false);
                    
                    SellTenButton.SetActive(false);
                    ReleaseButton.SetActive(false); 
                    OfferControlButton.SetActive(false);
                    PayDebt.SetActive(true);
                    return "Out of Business";
                }
            case global::BuildingStatus.BuildingCondition.Indie: 
                {
                    BuyTenButton.SetActive(false);
                    ActivateControlButtons(false);
                    SellTenButton.SetActive(false);
                    WashTheirMoney.SetActive(false);
                    PayDebt.SetActive(false);
                    if (b.bStatus.FellasRequired < b.bStatus.nSecurityFells) { ReleaseButton.SetActive(true); } else { ReleaseButton.SetActive(false); }
                    OfferControlButton.SetActive(true);
                    return "Independent"; 
                }
            case global::BuildingStatus.BuildingCondition.Controlled: 
                {
                    OfferControlButton.SetActive(false);
                    ReleaseButton.SetActive(true);
                    WashTheirMoney.SetActive(true);
                    ActivateControlButtons(true);
                    PayDebt.SetActive(false);
                    if (b.bStatus.Ownership < 1) { BuyTenButton.SetActive(true); } else { BuyTenButton.SetActive(false); }
                    if (b.bStatus.Ownership > 0) { SellTenButton.SetActive(true);  } else { SellTenButton.SetActive(false);  }
                    
                    return "Controlled"; 
                }
            case global::BuildingStatus.BuildingCondition.Owned: 
                {
                    if (b.bStatus.FellasRequired == 0)
                    {
                        OfferControlButton.SetActive(false);
                        ReleaseButton.SetActive(false);
                        WashTheirMoney.SetActive(true);
                        ActivateControlButtons(true);
                        PayDebt.SetActive(false);
                        if (b.bStatus.Ownership < 1) { BuyTenButton.SetActive(true); } else { BuyTenButton.SetActive(false); }
                        if (b.bStatus.Ownership > 0) { SellTenButton.SetActive(true); } else { SellTenButton.SetActive(false); }
                    }
                    else
                    {
                        WashTheirMoney.SetActive(false);
                        PayDebt.SetActive(false);
                        BuyTenButton.SetActive(false);
                        SellTenButton.SetActive(false);
                        OfferControlButton.SetActive(true);
                    }
                    
                } return "Owned"; 
            default: return "";
        }

    }

    private void ActivateControlButtons(bool p)
    {
        foreach(GameObject g in ControlButtons)
        {
            g.SetActive(p);
        }
    }


    public void BossMissionSwitch()
    {

       /* if (BossPanel.activeSelf)
        {
            
            BossPanel.SetActive(false);
            MissionPanel.SetActive(true);
        }
        else
        {

            if (MissionPanel.activeSelf)
            {
                
                MissionPanel.SetActive(false);
                BossPanel.SetActive(true);
            }
        }*/

    }

    public void AddNewLine(string newLine) 
    {
        //Debug.Log(newLine);
        if (linesCounter == 0)
        {
            lines[linesCounter++].GetComponent<Text>().text = newLine;
        }
        if (linesCounter == 1) 
        {
            lines[1].GetComponent<Text>().text = lines[0].GetComponent<Text>().text;
            lines[0].GetComponent<Text>().text = newLine;
            linesCounter++;
        }
        if (linesCounter >= 2)
        {
            lines[2].GetComponent<Text>().text = lines[1].GetComponent<Text>().text;
            lines[1].GetComponent<Text>().text = lines[0].GetComponent<Text>().text;
            lines[0].GetComponent<Text>().text = newLine;
            linesCounter++;
        }

        
    }

    //Panel Switching
    public void DisplayBuildingPanel(Building building)
    {
        if (BuildingPanel.activeInHierarchy)
        {
            if (BuildingName.GetComponent<Text>().text == building.name)
            {
                BuildingPanel.SetActive(false);
                //FellaPanel.SetActive(true);
                BuildingName.GetComponent<Text>().text = "";
                
            }
            else
            {
                BuildingPanel.SetActive(true);
                SetBuildingPanelValues(building);
                FellaPanel.SetActive(false);
                
            }
        }
        else
        {
            
            BuildingPanel.SetActive(true);
            SetBuildingPanelValues(building);
            if(FellaPanel.activeInHierarchy){
                FellaPanel.SetActive(false);
            }
            /*if (MissionPanel.activeInHierarchy)
            {
                MissionPanel.SetActive(false);
            }*/


        }
    }

    //Alert Control
    public void SetAlertString(string s) 
    {
        BuildingAlert.GetComponent<Text>().text = s;
    }

    public void ResetAlertString()
    {
        BuildingAlert.GetComponent<Text>().text = "";
    }



    internal void DisplayFellaPanel(Fella fella)
    {
        if (FellaPanel.activeInHierarchy)
        {
            if (FellaName.GetComponent<Text>().text == fella.name)
            {
                FellaPanel.SetActive(false);
                //FellaPanel.SetActive(true);
                FellaPanel.GetComponent<Text>().text = "";

            }
            else
            {
                FellaPanel.SetActive(true);
                SetFellaPanelValues(fella);
                BuildingPanel.SetActive(false);

            }
        }
        else
        {

            FellaPanel.SetActive(true);
            SetFellaPanelValues(fella);
            if (BuildingPanel.activeInHierarchy)
            {
                BuildingPanel.SetActive(false);
            }
            /*if (MissionPanel.activeInHierarchy)
            {
                MissionPanel.SetActive(false);
            }*/


        }
    }

    public void SetFellaPanelValues(Fella fella)
    {
        FellaName.GetComponent<Text>().text = "" + fella.name;
        FellaStatus.GetComponent<Text>().text = "Status : " + fella.GetFellaStatusString();
        Upgrades[3].GetComponent<Text>().text = "+ " + (fella.DrugDealerProb*100) + "% Black deal,\n+ 5$B per month";
        WeaponValue.GetComponent<Text>().text = "" + (fella.shotPower);
        HealthValue.GetComponent<Text>().text = "" + (fella.health);
        DisplayUpgrades(fella);
    }

    private void DisplayUpgrades(Fella fella)
    {
        
        switch (fella.internalStatus)
        {
            case Fella.FellaStatus.DrugDealer: 
                { 
                    Upgrades[0].GetComponent<Text>().text = "Disable";
                    Upgrades[1].GetComponent<Text>().text = "Unavailable";
                    Upgrades[2].GetComponent<Text>().text = "Unavailable"; 
                } 
                break;
            case Fella.FellaStatus.WeaponDealer:
                {
                    Upgrades[0].GetComponent<Text>().text = "Unavailable";
                    Upgrades[1].GetComponent<Text>().text = "Disable";
                    Upgrades[2].GetComponent<Text>().text = "Unavailable";
                }
                break;
            case Fella.FellaStatus.Guard:
                {
                    Upgrades[0].GetComponent<Text>().text = "Unavailable";
                    Upgrades[1].GetComponent<Text>().text = "Unavailable";
                    Upgrades[2].GetComponent<Text>().text = "Disable";
                }
                break;
            case Fella.FellaStatus.Standard:
                {
                    Upgrades[0].GetComponent<Text>().text = "Activate";
                    Upgrades[1].GetComponent<Text>().text = "Activate";
                    Upgrades[2].GetComponent<Text>().text = "Activate";
                }
                break;
        }
        
    }

    internal void SwitchMicroPanelFella(int i)
    {
        foreach(GameObject g in FellaMicroPanels)
        {
            if (g.activeInHierarchy)
            {
                g.SetActive(false);
            }
        }

        FellaMicroPanels[i].SetActive(true);

    }



    internal void SwitchMicroPanelBuilding(int i)
    {
        foreach (GameObject g in BuildingMicroPanels)
        {
            if (g.activeInHierarchy)
            {
                g.SetActive(false);
            }
        }

        BuildingMicroPanels[i].SetActive(true);

    }


}
