﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class SpawningManager : MonoBehaviour {

    Transform[] spawnPoints;

    public GameObject Citizen;
    public int MaxCitizen = 10;

    List<BaseAgent> CitizenPool;



	// Use this for initialization
	IEnumerator Start () {

        CitizenPool = new List<BaseAgent>();

        spawnPoints = GetComponentsInChildren<Transform>();

        

        while (CitizenPool.Count < MaxCitizen)
        {
            if (StaticMap.MapReady)
            {
                yield return StartCoroutine(FillPool());
                
            }
        }

        while (true) 
        {

            yield return StartCoroutine(SpawnCitizen());
            yield return new WaitForSeconds(2f);
        }




        yield return null;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator FillPool()
    {
        Transform spawnerPosition = spawnPoints[Random.Range(1, spawnPoints.Length)];

        

        GameObject g = (GameObject)Instantiate(Citizen, spawnerPosition.position, Quaternion.identity);

        g.GetComponent<BaseAgent>().startingPosition = spawnerPosition;

        g.transform.parent = transform;

        CitizenPool.Add(g.GetComponent<BaseAgent>());
        
        
        yield return null;
    }

    IEnumerator SpawnCitizen()
    {
        

        foreach ( BaseAgent citizen in CitizenPool)
        {
            if (!citizen.Active) 
            {
                Building b = StaticMap.GetRandomBuilding();

                Transform s = spawnPoints[Random.Range(1, spawnPoints.Length)];

                Debug.Log("Spawning : "+s.position);

                citizen.transform.position = s.position;
                citizen.startingPosition = s;

                citizen.TurnOn(b.GetDoor());

                break;
            }

        }

        
        //Budget

        yield return null;

    }

    

}
