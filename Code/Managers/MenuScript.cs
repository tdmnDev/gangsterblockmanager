﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

    public MissionManager missionManager;

    public GameObject[] Loading;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void StartGame() 
    {
        Application.LoadLevel("MissionScene");
    }

    public void Tutorial()
    {
        Application.LoadLevel("TutorialScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void EasyMission()
    {
        DB_MissionParamsContainer dbMission = DB_MissionParamsContainer.LoadFromStaticPath();
        
        Debug.Log("0");
        missionManager.ControlledBuilding = dbMission.vMissionParams[0].NumControlledBuilding;
        missionManager.OwnedBuilding = dbMission.vMissionParams[0].NumOwnedBuilding;
        missionManager.MinOwnership = dbMission.vMissionParams[0].MinOwnership;
        missionManager.monthDuration = dbMission.vMissionParams[0].MonthDuration;
        Loading[0].SetActive(true);
        Application.LoadLevel("GameScene");
    }

    public void IntermediateMission()
    {
        DB_MissionParamsContainer dbMission = DB_MissionParamsContainer.LoadFromStaticPath();

        Debug.Log("1");
        missionManager.ControlledBuilding = dbMission.vMissionParams[1].NumControlledBuilding;
        missionManager.OwnedBuilding = dbMission.vMissionParams[1].NumOwnedBuilding;
        missionManager.MinOwnership = dbMission.vMissionParams[1].MinOwnership;
        missionManager.monthDuration = dbMission.vMissionParams[1].MonthDuration;
        Loading[1].SetActive(true);
        Application.LoadLevel("GameScene");
    }

    public void HardMission()
    {
        DB_MissionParamsContainer dbMission = DB_MissionParamsContainer.LoadFromStaticPath();
        Debug.Log("2");
        missionManager.ControlledBuilding = dbMission.vMissionParams[2].NumControlledBuilding;
        missionManager.OwnedBuilding = dbMission.vMissionParams[2].NumOwnedBuilding;
        missionManager.MinOwnership = dbMission.vMissionParams[2].MinOwnership;
        missionManager.monthDuration = dbMission.vMissionParams[2].MonthDuration;
        Loading[2].SetActive(true);
        Application.LoadLevel("GameScene");
    }


}
