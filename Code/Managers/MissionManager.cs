﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MissionManager : MonoBehaviour {

    //Mission Goals
    public int ControlledBuilding;
    public int OwnedBuilding;
    public float MinOwnership;
    public int monthDuration;

    public PlayerScript player;
    public GameManager gManager;
    public bool Active = false;

    //Mission Panel
    public GameObject MissionPanel;
    private GameObject ControlledValue;
    private GameObject OwnedValue;
    private GameObject DurationValue;
    private GameObject MinOwnDesc;
    private GameObject MinOwnValue;
    public string DeathReason;


	// Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    void Update() 
    {
        if (Active && CheckVictory())
        {
            Application.LoadLevel("WinningScene");
        }

        if (Application.loadedLevel == 0)
        {
            Destroy(gameObject);
        }

        if (MissionPanel != null && MissionPanel.activeInHierarchy)
        {
            UpdateMissionPanel();
        }
    }

    private void UpdateMissionPanel()
    {
        ControlledValue.GetComponent<Text>().text = "" + player.ControlledBuilding.Count + "/" + ControlledBuilding;
        OwnedValue.GetComponent<Text>().text = "" + player.OwnedBuilding.Count + "/" + OwnedBuilding;
        DurationValue.GetComponent<Text>().text = "" + gManager.years+"/"+monthDuration;

    }

    public void SetMissionPanel(GameObject panel)
    {
        MissionPanel = panel;
        ControlledValue = MissionPanel.transform.FindChild("Mission-ControlledValue").gameObject;
        OwnedValue = MissionPanel.transform.FindChild("Mission-OwnedValue").gameObject;
        DurationValue = MissionPanel.transform.FindChild("Mission-DurationValue").gameObject;
        MinOwnDesc = MissionPanel.transform.FindChild("OwnershipDesc").gameObject;
        
    }

    public bool CheckVictory() 
    {
        if (gManager)
        {
            if (gManager.years >= monthDuration)
            {
                if (player.ControlledBuilding.Count >= ControlledBuilding)
                {
                    if (CheckOwnership())
                    {
                        return true;
                    }
                }
            }
        }
        

        return false;
    }

    private bool CheckOwnership()
    {
        int ownershipCounter = 0;
        if (player.OwnedBuilding.Count >= OwnedBuilding)
        {
            foreach (float f in player.Ownerships)
            {
                if (f >= MinOwnership)
                {
                    ownershipCounter++;
                }
            }

            if (ownershipCounter >= OwnedBuilding)
            {
                return true;
            }
        }

        return false;
    }

    private int nRequestedOwnership() 
    { 
        int ownershipCounter = 0;
        
        foreach (float f in player.Ownerships)
        {
            if (f >= MinOwnership)
            {
                ownershipCounter++;
            }
        }

        return ownershipCounter;
    }
	
}
