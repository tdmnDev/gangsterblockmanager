﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {


    public List<AudioClip> trackList;
    public AudioSource musicSpeaker;
    int TrackCounter = 0;

    public bool Active;

	// Use this for initialization
	void Start () {
        if (Active)
        {
            PlayNextTrack();
        }
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!musicSpeaker.isPlaying && Active) 
        {
            PlayNextTrack();
        }
	}

    void PlayNextTrack()
    {
        if (TrackCounter == trackList.Count) { TrackCounter = 0; }
        musicSpeaker.clip = trackList[TrackCounter++];
        musicSpeaker.Play();
    }
}
