﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;

public class PlayerScript : MonoBehaviour {

    public int BlackMoney;
    public int CleanMoney;
    public Color playerColor;
    
    //Fellas
    public List<GameObject> Fellas;
    public GameObject fellaPrefab;
    public int totalFellaCost = 0;
    public int FellaCost = 5;
    public int GuardCost = 20;
    public int DrugDealerCost = 10;
    public int WeaponDealerCost = 15;


    //Buildings Parameter
    public List<Building> ControlledBuilding;
    public List<Building> OwnedBuilding;
    public List<float> Ownerships;

    //Feedback
    public AudioSource CantPayFellasAudio;
    public AudioSource PayFellasAudio;

    //Stats
    public int TaxCounter = 0;
    public int FreeFellasCounter = 0;

	// Use this for initialization
	void Start () {

        Fellas = new List<GameObject>();
        ControlledBuilding = new List<Building>();
        OwnedBuilding = new List<Building>();
        Ownerships = new List<float>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void HireFella(bool free)
    {
        if (BlackMoney > FellaCost)
        {
            GameObject g = (GameObject)Instantiate(fellaPrefab, Vector3.zero, Quaternion.identity);
            Fellas.Add(g);
            g.transform.parent = gameObject.transform;
            g.transform.position = transform.position;
            if (!free)
            {
                BlackMoney -= FellaCost;
            }
            FreeFellasCounter++;
            g.name = RandomizeNames();
        }
        
        CalculateWages();
    }

    public bool ControlBuilding(Building b)
    {
        if (SendFellasToBuilding(b))
        {
            ControlledBuilding.Add(b);
            return true;
        }
        return false;
        

    }

    public bool SendFellasToBuilding(Building b)
    {
        int FellasRequired = b.bStatus.nSecurityFells - b.bStatus.securityFellas.Count;

        if (GetFreeFellas() >= FellasRequired)
        {
            foreach (GameObject g in Fellas)
            {
                if (g.GetComponent<Fella>().free) 
                {
                    g.GetComponent<Fella>().free = false;
                    g.GetComponent<Fella>().assignedBuilding = b.bStatus;
                    b.bStatus.securityFellas.Add(g);
                    g.transform.position = b.transform.position;
                    --FellasRequired;
                    --FreeFellasCounter;
                    
                }
                if (FellasRequired == 0) { break; }
            }
            b.DisposeFellas();
            return true;
        }
        return false;

    }

    public bool RecallFellasFromBuilding(Building b)
    {
        List<GameObject> gList = new List<GameObject>();
        
        foreach (GameObject g in b.bStatus.securityFellas)
        {
            if (!g.GetComponent<Fella>().free)
            {
                g.GetComponent<Fella>().free = true;
                g.GetComponent<Fella>().assignedBuilding = null;
                gList.Add(g);
                g.transform.position = gameObject.transform.position;
                ++FreeFellasCounter;

            }
            
        }

        foreach (GameObject g in gList)
        {
            b.bStatus.securityFellas.Remove(g);
        }
        b.bStatus.PlayerController = null;

        CalculateWages();

        return true;

    }

    public int GetFreeFellas()
    {
        
        return FreeFellasCounter;
    }

    internal bool PayFellas()
    {
        if (FellaCost * Fellas.Count < BlackMoney) 
        {
            BlackMoney -= FellaCost * Fellas.Count;
            PayFellasAudio.Play();
            return true;
        }
        return false;
        
    }

    public void RecivePayment(int BlackMoney, int CleanMoney) 
    {
        this.BlackMoney += BlackMoney;
        this.CleanMoney += CleanMoney;
    }

    public void WashFifty() { if (BlackMoney > 50) { BlackMoney -= 50; CleanMoney += 35; } }

    public void DirtyFifty() { if (CleanMoney > 50) { CleanMoney -= 50; BlackMoney += 35; } }

    internal bool PayTaxes()
    {
        int taxes=0;
        foreach (Building b in ControlledBuilding) 
        {
            taxes += Mathf.RoundToInt(b.bStatus.BuildingCost * b.bStatus.Ownership);
        }
        //taxes *=3;

        
        if (taxes <= CleanMoney) 
        {
            CleanMoney -= taxes;
            return true;
        }

        return false;

    }

    internal int CalculateTaxes()
    {
        int taxes = 0;
        foreach (Building b in ControlledBuilding)
        {
            taxes += Mathf.RoundToInt(b.bStatus.BuildingCost * b.bStatus.Ownership);
        }
        //taxes *= 3;
        return taxes;
    }

    internal bool BuyTenPercent(Building selectedBuilding)
    {
        int cost = (int)(selectedBuilding.bStatus.BuildingValue * 0.1);
        if (CleanMoney >= cost) 
        {
            CleanMoney -= cost;
            selectedBuilding.bStatus.Ownership += 0.1f;

            int index;
            if (!OwnedBuilding.Contains(selectedBuilding)) 
            {
                OwnedBuilding.Add(selectedBuilding);
                index = OwnedBuilding.Count - 1;
                Ownerships.Add(selectedBuilding.bStatus.Ownership);
            }
            else
            {
                index = OwnedBuilding.IndexOf(selectedBuilding);
                Ownerships[index] = selectedBuilding.bStatus.Ownership;
            }
            
            
            TaxCounter = CalculateTaxes();
            return true;
        }
        return false;
    }

    internal void SellTenPercent(Building selectedBuilding)
    {
        int cost = (int)(selectedBuilding.bStatus.BuildingValue * 0.1);
        
            CleanMoney += cost;
            selectedBuilding.bStatus.Ownership -= 0.1f;


            int index;
            if (selectedBuilding.bStatus.Ownership == 0)
            {
                index = OwnedBuilding.IndexOf(selectedBuilding);
                OwnedBuilding.Remove(selectedBuilding);
                Ownerships.RemoveAt(index);
            }
            else
            {
                index = OwnedBuilding.IndexOf(selectedBuilding);
                Ownerships[index] = selectedBuilding.bStatus.Ownership;
            }


            TaxCounter = CalculateTaxes();
        
    }

    internal bool ReleaseBuilding(Building selectedBuilding)
    {
        if (RecallFellasFromBuilding(selectedBuilding))
        {
            ControlledBuilding.Remove(selectedBuilding);
            return true;
        }
        return false;
    }

    internal bool WashTheirMoney(Building selectedBuilding)
    {
        if (selectedBuilding.bStatus.BlackCash > 50 && CleanMoney > 30)
        {
            selectedBuilding.bStatus.BlackCash -= 50;
            selectedBuilding.bStatus.CleanCash += 30;

            CleanMoney -= 30;
            BlackMoney += 50;

            return true;
        }

        return false;
    }

    internal void PayDebt(int p)
    {
        CleanMoney -= p;
    }

    public void CalculateWages()
    {
        int totalcost =0;
        foreach (GameObject g in Fellas)
        {
            switch (g.GetComponent<Fella>().internalStatus)
            {
                case Fella.FellaStatus.Standard: { totalcost += FellaCost; } break;
                case Fella.FellaStatus.DrugDealer: { totalcost += FellaCost+DrugDealerCost; } break;
                case Fella.FellaStatus.WeaponDealer: { totalcost += FellaCost+WeaponDealerCost; } break;
                case Fella.FellaStatus.Guard: { totalcost += FellaCost+GuardCost; } break;
            }
        }

        totalFellaCost = totalcost;
    }

    private string RandomizeNames()
    {
        string name = StaticMap.firstList[Random.Range(0, StaticMap.firstList.Length - 1)];
        name += " " + StaticMap.lastList[Random.Range(0, StaticMap.lastList.Length - 1)];
        return name;
    }
}
