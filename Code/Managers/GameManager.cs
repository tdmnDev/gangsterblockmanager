﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Map;


public class GameManager : MonoBehaviour {

    public GameObject cachedPlayer;
    public PlayerScript player = null;
    public GameObject MissionPanel; //Cached

    //Default starting Building
    Building startingBuilding;
    public GameObject MissionManager;
    
    //Game parameters
    public float wagesTime;
    public float taxesTime;
    public float years;
    public float timeWagesLimit;
    float timeTaxesLimit;

    //Audio Clips
    public AudioClip PayWages;
    public AudioClip CantPayWages;
    public AudioClip PayTaxes;
    public AudioClip CantPayTaxes;

    //Calendar                1   2   3   4   5   6   7   8   9   10  11  12
    public int[] monthLen = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    public string[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
    public int hourCounter = 0;
    public int dayCounter = 1;
    public int monthIndex = 0;
    private float[] TimeSpeed = {0.5f,20f,15f};
    public int TimeSpeedIndex=0;
    
    //Environmental Status
    private int EconomyIndex = 0;
    private int SeasonIndex = 0;

    //Active status
    bool Active = false;

    //private parameters
    int startingBuildingIndex = 4;



	// Use this for initialization
	IEnumerator Start () {
        MissionManager = GameObject.Find("MissionManager"); //used because MissionManager is a cross-level Object
        
        //Wait until scene is ready
        while (!StaticMap.MapReady) 
        {
            yield return null;
        }
        startingBuilding = StaticMap.buildings[startingBuildingIndex];

        //Setup Timers
        wagesTime = 0;
        taxesTime = 0;
        years = 0;
        timeWagesLimit = monthLen[monthIndex];
        dayCounter = 1;

        //Setup Player
        player = cachedPlayer.GetComponent<PlayerScript>();
        setupPlayer(player);
        setMission();

        Active = true;
        
    }

    //Set choosen mission
    private void setMission()
    {
        MissionManager mm = MissionManager.GetComponent<MissionManager>();
        mm.gManager = this;
        mm.player = player;
        mm.SetMissionPanel(MissionPanel);
        mm.Active = true;
    }

    //Setup player on start
    private bool setupPlayer(PlayerScript plScript)
    {
        plScript.transform.position = startingBuilding.transform.position;
        for (int i = 0; i < startingBuilding.bStatus.nSecurityFells; i++)
        {
            plScript.HireFella(true);
        }

        plScript.ControlBuilding(startingBuilding);

        return true;
    }

    //Used to update game time
    void FixedUpdate() 
    {
        
        //Update day counter
        if ( hourCounter > (int)((Time.fixedTime * TimeSpeed[TimeSpeedIndex]) % 24) && hourCounter == 23)
        {
            dayCounter++;
            wagesTime++;
            StaticMap.ResetDairyDeals(); //Reset diary deals of every building
        }
        hourCounter = (int)((Time.fixedTime * TimeSpeed[TimeSpeedIndex]) % 24);


        // Update Month and Year
        if (Active && dayCounter % monthLen[monthIndex] == 0) 
        {
            
                dayCounter = 1;
                monthIndex++;
                timeWagesLimit = monthLen[monthIndex];
                wagesTime = 0;
                MonthSchedule();
                StaticMap.TaxDayBuilding();

            if (monthIndex > monthLen.Length - 1) 
            {
                Debug.Log("new Year");
                monthIndex = 0;
            }
        }
    }

    public string getCurrentMonth() 
    {
        return monthName[monthIndex];
    }

    //Check Schedules
    private void TrimesterSchedule()
    {
        if (!player.PayTaxes())
        {
            //END GAME
            MissionManager.GetComponent<MissionManager>().DeathReason = "Not Enough Money for Taxes, one of your Fellas start shot you!";

            StaticMap.ClearBuildingList();
            Application.LoadLevel("LosingScene");

        }
    }
    private void MonthSchedule()
    {
            if (!player.PayFellas())
            {
                //END GAME
                MissionManager.GetComponent<MissionManager>().DeathReason = "Not Enough Money for Fellas, they killed you!";
                StaticMap.ClearBuildingList();
                Application.LoadLevel("LosingScene");
            }

            if ( !player.PayTaxes())
            {
                //END GAME
                MissionManager.GetComponent<MissionManager>().DeathReason = "Not Enough Money for Taxes, Police killed you!";
                StaticMap.ClearBuildingList();
                Application.LoadLevel("LosingScene");
            }
            years += 0.1f;

    }


    //Edit time scale during gameplay
    public void TimeScalePlus()
    {
        if (Time.timeScale < 3)
        {
            Time.timeScale += 1;
        }
    }

    public void TimeScaleMinus()
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale -= 1;
        }
    }


    //Edit gameplay parameters
    private void SeasonHandler() 
    { 
        //Winter
        if (monthIndex == 12)
        {
            //ChangeToWinter
        }

        //Spring
        if (monthIndex == 3)
        {
            //ChangeToSpring
        }

        //Summer
        if (monthIndex == 6)
        {
            //ChangeToSummer
        }

        //Autumn
        if (monthIndex == 9)
        {
            //ChangeToAutumn
        }

    }

    private void EconomyHandler() 
    { 
        //Flex probability
        int prob = Random.Range(-1, 2);

        Debug.Log("EconomyIndex : "+EconomyIndex+", Prob : "+prob+"");
        
        if (prob != 0 &&(((prob+EconomyIndex) >= -2) && ((EconomyIndex+prob) <= 2)))
        {
            
            EconomyIndex += prob;
            Debug.Log("Apply Economy " + EconomyIndex);
            StaticMap.ApplyNewEconomyIndex(EconomyIndex);
        }
        

    }
}
