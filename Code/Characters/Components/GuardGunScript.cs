﻿using UnityEngine;
using System.Collections;

public class GuardGunScript : MonoBehaviour {

    public SphereCollider collider;
    public Fella fellaScript;
    public ParticleSystem FireShot;

    void OnTriggerEnter(Collider other)
    {
        
        if (fellaScript.internalStatus == Fella.FellaStatus.Guard) 
        {
            KillerAgentScript killer = other.GetComponent<KillerAgentScript>();
            ThiefAgentScript thief = other.GetComponent<ThiefAgentScript>();
            if(killer != null || thief != null)
            {
                
                transform.parent.LookAt(other.transform.position);
                FireShot.Play();
                if (killer != null)
                {
                    killer.health = 0;
                }

                if (thief != null)
                {
                    thief.health = 0;
                }
                
            }
        }
    }
}
