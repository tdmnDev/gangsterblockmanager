﻿using UnityEngine;
using System.Collections;

public class KillerAgentScript : MonoBehaviour {


    //Movement
    public Transform startingPosition = null;
    public Transform target = null;
    NavMeshAgent agent;


    //Status
    public bool Active = false;
    private bool Running = false;
    private float runningCounter = 0;

    //Attack
    public bool Attacking = false;
    public int EnemyIndex;
    public float health = 10;
    public float shotPower = 1f;
    public BaseAgent citizenTarget;
    public SphereCollider range;
    private float ShotCooldown = 1f;
    private float ShotCooldownCounter = 1f;

    //Feedback
    public ParticleSystem ParticleFeedback;
    public AudioSource Gun;
    public GameObject BloodSplat;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null && Active)
        {
            agent.SetDestination(target.position);
            if (GetDestinationDistance() < 10)
            {
                TurnOff();
            }
        }

        if (health <= 0)
        {
            Instantiate(BloodSplat, transform.position, transform.rotation);
            TurnOff();
        }

       

    }

    void OnTriggerEnter(Collider other)
    {


        if (Active)
        {
            //Thief - Rob Citizen
            BaseAgent citizen = null;
            citizen = other.GetComponent<BaseAgent>();
            if (citizen != null && Active)
            {
                transform.LookAt(other.transform);
                citizenTarget = citizen;
                ShootTarget();
                
            }


        }
    }

    void TurnOff()
    {

        Destroy(gameObject);
    }

    public void TurnOn()
    {
        gameObject.layer = 15;
        range.enabled = true;
        gameObject.GetComponent<Renderer>().enabled = true;

        Active = true;
        Attacking = false;
    }

    internal void SetDirection(Transform transform)
    {
        target = transform;
    }



    internal void ReciveAttack(float p)
    {
        health -= p;
    }

    internal void BackToHome()
    {
        
        Attacking = false;
        
        target = startingPosition;
        Active = true;
        Running = true;
    }


    private float GetDestinationDistance()
    {
        float t = Vector3.Distance(transform.position, target.position);
        
        return t;
    }

    private void ShootTarget()
    {
        //Turning
        transform.LookAt(citizenTarget.transform);

        //Shot
        citizenTarget.ReciveShot(5);
        ParticleFeedback.Play();

    }


}
