﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Controller : MonoBehaviour {

    public GameObject player;
    private PlayerScript plScript;

    public GUImanager guiManager;

    public Building selectedBuilding;
    public Fella selectedFella;

    private float AlertTime = 3f;
    private float AlertTimeCounter = 3f;

    //Top Buttons
    public GameObject PauseTextButton;
    public GameObject PausePanel;
    public int GuardCost;
    public int WeaponDealerCost;
    public int DrugDealerCost;

	// Use this for initialization
	void Start () {
        selectedBuilding = null;
        plScript = player.GetComponent<PlayerScript>();
	}
	
	// Update is called once per frame
	void Update () {
        if (AlertTimeCounter > 0)
        {
            AlertTimeCounter += Time.deltaTime;
            if (AlertTimeCounter >= AlertTime)
            {
                AlertTimeCounter = 0;
                guiManager.ResetAlertString();
            }
        }
        
	}

    public void PauseResume() 
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            PauseTextButton.GetComponent<Text>().text = "Pause / Tutorial";
            PausePanel.SetActive(false);
        }
        else
        {
            PauseTextButton.GetComponent<Text>().text = "Resume";
            PausePanel.SetActive(true);
            Time.timeScale = 0;
            
        }
    }


    internal void HandleBuildingClick(Building building)
    {
        if (selectedFella != null) { selectedFella.SelectLight(); selectedFella = null; }
        if (selectedBuilding != building )
        {
            
            if (selectedBuilding != null) { selectedBuilding.SelectLight(); }
            selectedBuilding = building;
            selectedBuilding.SelectLight();
            
        }
        else
        {
            
            selectedBuilding.SelectLight();
            selectedBuilding = null;

            
        }

        guiManager.DisplayBuildingPanel(building);

        
        
    }

    internal void HandleFellaClick(Fella fella)
    {
        if (selectedBuilding != null) { selectedBuilding.SelectLight(); selectedBuilding = null; }

        if (selectedFella != fella)
        {

            if (selectedFella != null) { selectedFella.SelectLight(); }
            selectedFella = fella;
            selectedFella.SelectLight();

        }
        else
        {

            selectedFella.SelectLight();
            selectedFella = null;


        }

        guiManager.DisplayFellaPanel(fella);
    }

    public void BackToBossPanel()
    {
        if (selectedBuilding != null)
        {
            HandleBuildingClick(selectedBuilding);
        }
        
    }

    public void ControlBuilding()
    {
        if (!plScript.ControlBuilding(selectedBuilding))
        {
            guiManager.SetAlertString("Not Enough Fellas");
            AlertTimeCounter += Time.deltaTime;

        }
    }

    public void BackToIndie()
    {
        if (!plScript.ReleaseBuilding(selectedBuilding))
        {
            guiManager.SetAlertString("Can't recall, you own it");
            AlertTimeCounter += Time.deltaTime;

        }
    }

    public void WashTheirMoney()
    {
        if (!plScript.WashTheirMoney(selectedBuilding))
        {
            guiManager.SetAlertString("Not enough money to wash");
            AlertTimeCounter += Time.deltaTime;
            
        }
    }

    public void PayTheirDebt()
    {
        //
        if (plScript.CleanMoney > (selectedBuilding.bStatus.CleanCash * -1) && selectedBuilding.bStatus.CleanCash<0)
        {
            ControlBuilding();
            int debt = (0 - selectedBuilding.bStatus.CleanCash);
            int ownershipRequired = (selectedBuilding.bStatus.BuildingValue % debt) + 1;
            for (int i = 0; i < ownershipRequired; i++)
            {
                if (selectedBuilding.bStatus.Ownership == 1) { break; }
                BuyTenPercent();

            }

            if (selectedBuilding.bStatus.CleanCash < 0) 
            { 
                plScript.PayDebt((selectedBuilding.bStatus.CleanCash * -1)); 
                selectedBuilding.bStatus.CleanCash = 0; 
            }

        }


    }

    public void BuyTenPercent()
    {
        if(selectedBuilding.bStatus.Ownership < 1)
        {
            if (!plScript.BuyTenPercent(selectedBuilding))
            {
                guiManager.SetAlertString("Not Enough Clean Money");
                AlertTimeCounter += Time.deltaTime;
            }
            else
            {
                selectedBuilding.bStatus.UpdateTaxes();
            }
        }
        
    }

    public void SellTenPercent()
    {
        if (selectedBuilding.bStatus.Ownership > 0)
        {
            plScript.SellTenPercent(selectedBuilding);
        }
        else
        {
            selectedBuilding.bStatus.UpdateTaxes();
        }

    }


/*******  FELLA PANEL **********************/

    // + / - Button Controller 

    // add tax update
    public void PlusOpenHour() 
    {
        if (selectedBuilding.openHour < 23)
        {
            selectedBuilding.openHour += 1;
        }
        else { selectedBuilding.openHour = 0; }

        selectedBuilding.bStatus.UpdateTaxes();
    }    
    public void MinusOpenHour() 
    {
        if (selectedBuilding.openHour > 1)
        {
            selectedBuilding.openHour -= 1;
        }
        else { selectedBuilding.openHour = 23; }

        selectedBuilding.bStatus.UpdateTaxes();
    }
    public void PlusCloseHour()
    {
        if (selectedBuilding.closeHour < 23)
        {
            selectedBuilding.closeHour += 1;
        }
        else { selectedBuilding.closeHour = 0; }

        selectedBuilding.bStatus.UpdateTaxes();
    }
    public void MinusCloseHour()
    {
        if (selectedBuilding.closeHour > 1)
        {
            selectedBuilding.closeHour -= 1;
        }
        else { selectedBuilding.closeHour = 23; }

        selectedBuilding.bStatus.UpdateTaxes();
    }
    
    //add appeal upgrade
    public void PlusAvgCost()
    {
        if(selectedBuilding.bStatus.AvgCost < selectedBuilding.bStatus.MaxAvgCost)
        {
            selectedBuilding.bStatus.AvgCost += 1;
            selectedBuilding.effectiveAppeal -= 0.02f;
        }


    }
    public void MinusAvgCost()
    {
        if (selectedBuilding.bStatus.AvgCost > selectedBuilding.bStatus.MinAvgCost ) 
        {
            selectedBuilding.bStatus.AvgCost -= 1;
            selectedBuilding.effectiveAppeal += 0.02f;
        }
        
    }



    public void HandleFbUpgrade()
    {
        if (!selectedBuilding.HandleFbUpgrade())
        {
            //Add alert pop
            Debug.Log("Not enought money for fb upgrade");
        } 
    }

    public void HandleMascotteUpgrade()
    {
        if (!selectedBuilding.HandleMascotteUpgrade())
        {
            //Add alert pop
            Debug.Log("Not enought money for fb upgrade");
        }
    }


    //SwitchInternalPanels
    public void SwitchMicroPanelFella(int i)
    {
        guiManager.SwitchMicroPanelFella(i);
    }
    public void SwitchMicroPanelBuilding(int i)
    {
        guiManager.SwitchMicroPanelBuilding(i);
    }

    // Fella Upgrade Controller
    public void DrugDealerButton()
    {
        
        if (selectedFella.internalStatus == Fella.FellaStatus.Standard)
        {
            if (player.GetComponent<PlayerScript>().BlackMoney > selectedFella.DrugDealerCost)
            {
                selectedFella.UpgradeDrugDealer();
                player.GetComponent<PlayerScript>().BlackMoney -= DrugDealerCost;
            }
        }
        else
        {
            if (selectedFella.internalStatus == Fella.FellaStatus.DrugDealer)
            {
                selectedFella.DowngradeDrugDealer();
            }
        }

        guiManager.SetFellaPanelValues(selectedFella);
    }

    public void WeaponDealerButton()
    {
        if (selectedFella.internalStatus == Fella.FellaStatus.Standard)
        {
            if (player.GetComponent<PlayerScript>().BlackMoney > selectedFella.WeaponDealerCost)
            {
                selectedFella.UpgradeWeaponDealer();
                player.GetComponent<PlayerScript>().BlackMoney -= WeaponDealerCost;
                
            }
        }
        else
        {
            if (selectedFella.internalStatus == Fella.FellaStatus.WeaponDealer)
            {
                selectedFella.DowngradeWeaponDealer();
            }
        }

        guiManager.SetFellaPanelValues(selectedFella);
    }

    public void GuardButton()
    {
        if (selectedFella.internalStatus == Fella.FellaStatus.Standard)
        {
            if (player.GetComponent<PlayerScript>().BlackMoney > selectedFella.GuardCost)
            {
                selectedFella.UpgradeToGuard();
                player.GetComponent<PlayerScript>().BlackMoney -= GuardCost;
                
            }
        }
        else
        {
            if (selectedFella.internalStatus == Fella.FellaStatus.Guard)
            {
                selectedFella.DowngradeToGuard();
            }

        }
        guiManager.SetFellaPanelValues(selectedFella);
    }


    //Debug Method
    public void WeaponUpgradeButton()
    {
        if (player.GetComponent<PlayerScript>().BlackMoney > 50)
        {
            player.GetComponent<PlayerScript>().BlackMoney -= 50;
            selectedFella.shotPower += 0.5f;

        }

    }


}
