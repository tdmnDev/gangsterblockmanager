﻿using UnityEngine;
using System.Collections;

public class EnemyAgentScript : MonoBehaviour
{

    //Movement
    public Transform startingPosition = null;
    public Transform target = null;
    NavMeshAgent agent;


    //Status
    public bool Active = false;
    private bool Running = false;
    private float runningCounter = 0;
    
    //Attack
    public bool Attacking = false;
    public int EnemyIndex;
    public float health = 10;
    public float shotPower = 1f;
    public GameObject FellaTarget;
    private float ShotCooldown = 1f;
    private float ShotCooldownCounter = 1f;

    //Feedback
    public ParticleSystem ParticleFeedback;
    public AudioSource Gun;
    public GameObject BloodSplat;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null && Active)
        {
            agent.SetDestination(target.position);
        }
        
        if (health <= 0)
        {
            Instantiate(BloodSplat, transform.position, transform.rotation);
            TurnOff();
        }

        if (Running)
        {
            runningCounter += Time.deltaTime;
            if (runningCounter > 7)
            {
                runningCounter = 0;
                Running = false;
                TurnOff();
            }
            
        }

        

        if (Attacking && FellaTarget!= null && FellaTarget.activeInHierarchy)
        {
            ShotCooldownCounter -= Time.deltaTime;

            if (ShotCooldownCounter <= 0)
            {
                FellaTarget.GetComponent<Fella>().ReciveAttack(Random.Range(0f, shotPower), gameObject);
                ParticleFeedback.Play();
                Gun.Play();
                ShotCooldownCounter = Random.Range(0,2*ShotCooldown);
                
                if (FellaTarget.GetComponent<Fella>().health <=0)
                {
                    
                    BackToHome();
                }
            }
            
        }

    }

    void OnTriggerEnter(Collider other)
    {

        
        if (Active)
        {
            
            Building b = null;
            b = other.transform.parent.transform.parent.GetComponent<Building>();
            if (other.transform.position == target.position && b != null)
            {
                
                FellaTarget = b.GetFellaTarget(EnemyIndex);
                Attacking = true;
                target = null;
                
                
                
            }
        }
    }

    void TurnOff()
    {
        Active = false;
        gameObject.GetComponent<Renderer>().enabled = false;
        transform.position = startingPosition.position;
        gameObject.SetActive(false);
        target = null;
        health = 10;
        Attacking = false;
    }

    public void TurnOn()
    {
        gameObject.GetComponent<Renderer>().enabled = true;
        
        Active = true;
        Attacking = false;
    }

    internal void SetDirection(Transform transform)
    {
        target = transform;
    }



    internal void ReciveAttack(float p)
    {
        health -= p;
    }

    internal void BackToHome()
    {
        if (FellaTarget != null) { FellaTarget.GetComponent<Fella>().EnemyRan(); }
        Attacking = false;
        FellaTarget = null;
        Debug.Log("->"+startingPosition.position);
        target = startingPosition;
        Active = true;
        Running = true;
    }
}
