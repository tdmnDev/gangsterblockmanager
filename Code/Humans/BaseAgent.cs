﻿using UnityEngine;
using System.Collections;

public class BaseAgent : MonoBehaviour {

    //Movement
    public Transform startingPosition = null;
    public Transform target = null;
    NavMeshAgent agent;
    private bool Running = false;
    private float runningCounter = 0;
    
    //Status
    private int clearBudget;
    private int blackBudget;
    private int Wallet;
    public bool Active = false;
    private int robbery = 1;
    private float health = 5;
	
    //Feedback
    public ParticleSystem ParticleFeedback;
    public AudioSource CashFX;
    public GameObject BloodSplat;

    //Icon
    public Sprite Icon = null;
    public GameObject IconObject;
    private float IconCountDown = 5;
    private float IconCounter = 0;
    public Sprite SadIcon;

    // Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        //CashFX = transform.parent.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        //Set New Icon
        if (Icon != null && !Running)
        {
            IconObject.GetComponent<SpriteRenderer>().sprite = Icon;
        }

        //Set Destination
        if (target != null && Active)
        {
            agent.SetDestination(target.position);
        }

        //Death
        if (health <= 0)
        {
            Instantiate(BloodSplat, transform.position, transform.rotation);
            TurnOff();
        }

        //Turn off agent on target reach
        if (Running)
        {
            runningCounter += Time.deltaTime;
            if (GetDestinationDistance() < 10)
            {
                runningCounter = 0;
                Running = false;
                TurnOff();
            }

        }
        
	}

    /* Colliders are used to check when an agent reach a building target (door) */
    void OnTriggerEnter(Collider other) 
    {
        if (Active)
        {
            Building b = null;
            b = other.transform.parent.GetComponent<Building>();
            if (other.transform.position == target.position && b != null)
            {
                if (b.open)
                {
                    SetBudget(b.bStatus.AvgCost, b.bStatus.BlackMoneyProbability, b.bStatus.CleanMoneyProbability);
                    target = null;

                    //CashFX.Play();
                    other.GetComponent<ParticleSystem>().Play();
                    if (b.bStatus.RecivePayment(blackBudget, clearBudget))
                    {
                        TurnOff();
                    }
                    else
                    {
                        BackToHome();
                    }
                    
                }
                else
                {
                    //Debug.Log("Nooo! it's closed!");
                    BackToHome();
                }
            }
        }

        
    }

    //Turn Off Agent
    void TurnOff()
    {
        IconObject.GetComponent<SpriteRenderer>().sprite = null;
        Active = false;
        gameObject.GetComponent<Renderer>().enabled = false;
        transform.position = startingPosition.position;
        gameObject.SetActive(false);
        target = null;
    }

    //Turn On Agent
    public void TurnOn(Transform t) 
    {
        health = 3;
        gameObject.GetComponent<Renderer>().enabled = true;
        target = t;
        Active = true;
    }

    internal void SetDirection(Transform transform)
    {
        target = transform;
    }

    //Set Random wallet
    internal void SetBudget(int AvgCost, float blackProb, float cleanProb)
    {
        Wallet = Random.Range(0,AvgCost*2);
        float minProb = Mathf.Min(blackProb, cleanProb);
        float prob = Random.Range(0f, 1f);
        
        if (minProb <= prob)
        {
            
            if (minProb == blackProb )
            {

                clearBudget = Wallet;
                blackBudget = 0;
            }
            else
            {
                blackBudget = Wallet;
                clearBudget = 0;
            }
        }
        else
        {
            
            if (minProb == cleanProb)
            {
                blackBudget = 0;
                clearBudget = Wallet;
            }
            else
            {
                clearBudget = 0;
                blackBudget = Wallet;
            }
        }
    }

    internal void BackToHome()
    {
        //Debug.Log("->" + startingPosition.position);
        target = startingPosition;
        //Active = true;
        Running = true;
        IconObject.GetComponent<SpriteRenderer>().sprite = SadIcon;
    }

    internal void GetRobbed(int money)
    {
        robbery += money;
    }

    internal void ReciveShot(int p)
    {
        health -= p;
    }

    private float GetDestinationDistance()
    {
        float t = Vector3.Distance(transform.position, startingPosition.position);

        return t;
    }


}
