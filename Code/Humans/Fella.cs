﻿using UnityEngine;
using System.Collections;

public class Fella : MonoBehaviour {

    public bool free = true;
    public BuildingStatus assignedBuilding;

    public bool Attacking = false;
    public float health = 10;
    public float shotPower = 1f;
    public GameObject EnemyTarget = null;
    private float ShotCooldown = 1f;
    private float ShotCooldownCounter = 1f;

    //Feedback
    public ParticleSystem ParticleFeedback;
    public AudioSource ShotGun;
    public GameObject light;
    public GameObject BloodSplat;

    //Fella Upgrade
    public enum FellaStatus { Standard, DrugDealer, WeaponDealer, Guard };
    public FellaStatus internalStatus = FellaStatus.Standard;
    private PlayerScript bossPlayer;
    private Controller controller;
    //Fella Upgrade parameter
    public float DrugDealerProb;
    public float WeaponDealerProb;
    public int DrugDealerCost;
    public int WeaponDealerCost;
    public int GuardCost;

	// Use this for initialization
	void Start () {
        light.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        if (bossPlayer == null && transform.parent != null)
        {
            bossPlayer = transform.parent.GetComponent<PlayerScript>();
            controller = bossPlayer.transform.parent.GetComponent<Controller>();
        }

        //Return to Standard if Free
        if (free && internalStatus != FellaStatus.Standard)
        {
            internalStatus = FellaStatus.Standard;
        }

        //Attack
        if (EnemyTarget != null && EnemyTarget.activeInHierarchy && Attacking)
        {
            transform.LookAt(EnemyTarget.transform);
            ShotCooldownCounter -= Time.deltaTime;
            if (ShotCooldownCounter <= 0) 
            {
                EnemyTarget.GetComponent<EnemyAgentScript>().ReciveAttack(Random.Range(0f, shotPower));
                ParticleFeedback.Play();
                ShotGun.Play();
                ShotCooldownCounter = Random.Range(0,2*ShotCooldown);
                
            }
            
        }

        //Stop Attack
        if (EnemyTarget == null || !EnemyTarget.activeInHierarchy)
        {
            EnemyTarget = null;
            Attacking = false;
        }

        //Die
        if (health <= 0)
        {
            bossPlayer.Fellas.Remove(gameObject);
            assignedBuilding.securityFellas.Remove(gameObject);
            Instantiate(BloodSplat, transform.position, transform.rotation);
            Destroy(gameObject);
        }

        //Recharge Health
        if (health < 10 && !Attacking)
        {
            health += 0.0001f;
            if (health > 10)
            {
                health = 10;
            }
        }
	}

    public void ReciveAttack(float shot, GameObject Enemy) 
    {
        EnemyTarget = Enemy;
        health -= shot;
        Attacking = true;
        
    }

    internal void EnemyRan()
    {
        EnemyTarget = null;
        Attacking = false;
    }


    /* Upgrade */
    public void UpgradeDrugDealer()
    {
        assignedBuilding.BlackMoneyProbability += DrugDealerProb;
        assignedBuilding.CleanMoneyProbability -= DrugDealerProb;
        internalStatus = FellaStatus.DrugDealer;
        bossPlayer.CalculateWages();
    }

    public void UpgradeWeaponDealer()
    {
        assignedBuilding.BlackMoneyProbability += WeaponDealerProb;
        assignedBuilding.CleanMoneyProbability -= WeaponDealerProb;
        internalStatus = FellaStatus.WeaponDealer;
        bossPlayer.CalculateWages();
    }

    public void UpgradeToGuard()
    {
        internalStatus = FellaStatus.Guard;
        bossPlayer.CalculateWages();
    }

    /* Downgrade */
    public void DowngradeDrugDealer()
    {
        assignedBuilding.BlackMoneyProbability -= DrugDealerProb;
        assignedBuilding.CleanMoneyProbability += DrugDealerProb;
        internalStatus = FellaStatus.Standard;
        bossPlayer.CalculateWages();
    }

    public void DowngradeWeaponDealer()
    {
        assignedBuilding.BlackMoneyProbability -= WeaponDealerProb;
        assignedBuilding.CleanMoneyProbability += WeaponDealerProb;
        internalStatus = FellaStatus.Standard;
        bossPlayer.CalculateWages();
    }

    public void DowngradeToGuard()
    {
        internalStatus = FellaStatus.Standard;
        bossPlayer.CalculateWages();
    }



    public void RestoreStandard()
    {
        internalStatus = FellaStatus.Standard;
        bossPlayer.CalculateWages();
    }

    void OnMouseDown()
    {
        controller.HandleFellaClick(this);
    }

    public void SelectLight()
    {
        if (light.activeSelf)
        {

            light.SetActive(false);

        }
        else
        {

            light.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (internalStatus == FellaStatus.Guard)
        {
            //KillThief();
        }
    }

    internal string GetFellaStatusString()
    {
        switch (internalStatus)
        {
            case Fella.FellaStatus.Standard: { return "Standard Fella"; }
            case Fella.FellaStatus.DrugDealer: { return "Drug Dealer Fella"; } 
            case Fella.FellaStatus.WeaponDealer: { return "Weapon Dealer Fella"; } 
            case Fella.FellaStatus.Guard: { return "Guard Fella"; } 
            default: return "";
        }
    }
}
