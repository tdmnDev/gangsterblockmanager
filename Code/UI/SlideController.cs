﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SlideController : MonoBehaviour {

    public Sprite[] slides;
    
    public GameObject panel;
    public int SlideIndex = 0;
    private int SlideMax;

    void Start()
    {
        SlideMax = slides.Length;
        panel.GetComponent<Image>().sprite = slides[SlideIndex];
    }

    public void Next()
    {
        SlideIndex++;
        if (SlideIndex > SlideMax-1)
        { SlideIndex = 0; }
        //SetSlide 
        panel.GetComponent<Image>().sprite = slides[SlideIndex];
    }

    public void Prev()
    {
        SlideIndex--;
        if (SlideIndex < 0)
        { SlideIndex = SlideMax-1; }
        //SetSlide
        panel.GetComponent<Image>().sprite = slides[SlideIndex];
    }

    public void MainMenu()
    {
        Application.LoadLevel("StartMenuScene");
    }
}
