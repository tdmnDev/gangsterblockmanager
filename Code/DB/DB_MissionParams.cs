﻿using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using Path;

public class DB_MissionParams
{
    [XmlAttribute("Name")]
    public string MissionName;

    public int NumControlledBuilding;
    public int NumOwnedBuilding;
    public float MinOwnership;
    public int MonthDuration;
}

[XmlRoot("EndConditionCollection")]
public class DB_MissionParamsContainer
{
    [XmlArray("EndConditionList")]
    [XmlArrayItem("EndCondition")]
    public List<DB_MissionParams> vMissionParams = new List<DB_MissionParams>();


    public static DB_MissionParamsContainer LoadFromStaticPath()
    {
        var serializer = new XmlSerializer(typeof(DB_MissionParamsContainer));
        using (var stream = new FileStream(StaticPathDefinitions.DB_MissionParamsPath, FileMode.Open))
        {
            return serializer.Deserialize(stream) as DB_MissionParamsContainer;
        }
    }

}
